module.exports = {
  "globDirectory": "dist/",
  "globPatterns": [
    "**/*.{html,css,js,webmanifest,svg,png}"
  ],
  "swDest": "dist/service-worker.js",
  "navigateFallback": "/index.html"
}
