#bottlecapchallange mobile web game

## Introduction

This PWA project is an online mobile web game adaptation based on the #bottlecapchallange trending on social media. It was built using ParcelJS and ReactJS. The game is optimized for mobile devices **ONLY**

## Demo

You can play a live version of the concept game by scanning the QR Code using your mobile device.

![alt Scane the QR CODE](https://chart.googleapis.com/chart?cht=qr&chl=https%3A%2F%2Fbottlecapchallengegame.surge.sh%2F&chs=180x180&choe=UTF-8&chld=L%7C2)

## Instructions

# Game Play

To play, hold the mobile device in portrait mode and swipe the bottle caps in a right to left motion. Swiping too fast, slow or in the wrong area or direction may decrease your attempts. You have 3 attempts. Share your scores with your friends on Facebook.

## Installation

# Development

The project is development in ReactJS and uses a ParcelJS server and compiler with Workbox for PWA. You will need to set a FB_API_KEY and PUBLIC_URL env variable for the Facebook SDK

1. Clone the repo
2. Run `yarn install` or `npm install` to install dependencies
3. Run `yarn start` or `npm run start` to start the dev server at https://localhost:3000( or at PUBLIC_URL if environment variable is set)

# Production

1. Run `yarn build` or `npm run build` in *NODE_ENV=production* with *PUBLIC_URL* set to production server url and FB_API_KEY for Facebook SDK

## Bugs/ Issues

* Currently I'm learning ReactJS and I'm a bit awkward with the way images/ assets are reference.

## TODO

* Fix asset management
* Implement dynamic environment variables based on environment
* Optimize offline mode
* Implement Leaderboard
* Develop better graphics and animations

## Challenge

I'm issuing a challenge to ALL who dare, to participate in the #bottlecapchallenge programmer/ web developer style! Either fork the repo and create a better version or implement your own #bottlecapchallenge game using any tool you see fit. It also does not have to be a game but maybe a framework/ library, social app, website or utility etc. But it MUST built using code and should involve the #bottlecapchallenge theme/ concept/ idea.

# Good Luck and enjoy!

## License
