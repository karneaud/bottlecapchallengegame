import Stopwatch from '../lib/stopwatch.js'
import React from 'react';
import { connect } from 'react-redux';
import { setTime } from '../store/actions';

class StopwatchWidget extends React.Component {

  constructor(props) {
      super(props);

      this.state = {
        time: '00:00:00'
      }

      this.startStopwatch.bind(this)
      this.stopStopwatch.bind(this)

      this.props.stopwatch.tickHandler = (timestamp) => {
        let time = new Date(timestamp)
        this.setState({time:  this.format(timestamp) })
      }
  }

  componentDidMount() {
    this.props.stopwatch.reset()
  }

  format(time) {
    let t = new Date(time), minutes = t.getMinutes().toString(), seconds = t.getSeconds().toString(), milliseconds = (t.getMilliseconds()/  10).toFixed(0);

    minutes = minutes.padStart(2, '0');
    seconds = seconds.padStart(2, '0');
    milliseconds = milliseconds.padStart(2, '0');

    return minutes.concat(' : ', seconds , ' : ',  milliseconds);
  }

  stopStopwatch() {
    this.props.stopwatch.stop()
    this.props.setTime(this.state.time)
  }

  startStopwatch() {
    this.props.stopwatch.start()
  }

  render() {
    return (
      <span className="timer">
        TIME: <strong id="time">{ this.state.time }</strong>
    </span>
    );
  }
}

StopwatchWidget.defaultProps = {
  stopwatch: new Stopwatch()
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setTime: (time) => dispatch(setTime(time))
  }
}

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(StopwatchWidget)
