import React from 'react';
import tocca from "tocca";
import Siema from 'siema';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { decrementTries, incrementOpened } from '../store/actions'

const f = e => {
  e.preventDefault()
}, ff = e => {
  time = e.timeStamp ;
  startY = e.changedTouches[0].pageY
}, canOpenBottle = (params) => {
  switch (true) {
    case !params.swipeTime.between(.12,.45) :
    case !params.swipeDistPercent.between(28,60) :
    case !params.swipeAngle.between(-1, 15) :
    case !params.swipePosPercent.between(25,38) :
      return false
    default:
      return true
  }
}

let time = 0, startY = 0, currentSlide = 0, hold = false;


class GameObjects extends React.Component {

   constructor(props) {
     super(props)

     this.state = {
       currentSlide: 0,
       opened: false,
       unopened: false
     }

     this.objects = [ ]
     this.slider = null
     this.onGesture = this.onGesture.bind(this)
     this.onAnimationEnd = this.onAnimationEnd.bind(this)
   }

   onAnimationEnd(e){
      hold = false
      e.target.style.animation='none';
      setTimeout(() => e.target.style.animation='', 50);
      this.setState({ opened: false, unopened: false, currentSlide })
   }

   componentDidMount() {
     this.slider = new Siema({
       selector: '.slider',
       draggable: false,
       multipleDrag: false,
       duration: 1500,
       loop: true,
       onChange: () => {
         currentSlide = this.slider.currentSlide
       }
     })



     this.objects  = document.querySelectorAll('.bottle')
     window.addEventListener('touchmove', f, { passive: false })
     window.addEventListener('touchstart', ff, { passive: true })
     this.objects.forEach((el) => {
       el.addEventListener('swipeleft', (e) =>{
         if(hold) return false
         this.onGesture(e)
       })
       el.querySelector('.beer').addEventListener('webkitAnimationEnd', this.onAnimationEnd)
       el.addEventListener('webkitAnimationEnd', this.onAnimationEnd)
     })
   }

   componentWillUnmount() {
     window.removeEventListener('touchmove', f)
     window.removeEventListener('touchstart', ff )
   }

   onGesture(e) {
     let {  distance: { x } , timeStamp } = e, swipeDistPercent = (x/ window.screen.availWidth * 100), swipeAngle = Math.abs(Math.round(startY - e.originalEvent.changedTouches[0].pageY)),
     swipeTime = parseFloat(((timeStamp - time)/ 1000).toFixed(2)), swipePosPercent = (startY / e.srcElement.offsetHeight) * 100, can = canOpenBottle({
       swipeAngle,
       swipePosPercent,
       swipeTime,
       swipeDistPercent
     })
     hold = true
     this.setState({ opened: can, unopened: !can })
   }

   getClassNames(classes) {
     return classNames(classes);
   }

   componentDidUpdate(prevProps, prevState) {
      if(this.state.opened){
        this.slider.next()
        this.props.incrementOpened()
      } else if(this.state.unopened) {
        this.props.decrementTries()
      }
    }

   render() {
     return (
        <div className="slider">
          <div tabIndex="0" id="bottle-1" key="bottle-1" className={`${this.getClassNames({'bottle': true,'fall': hold && this.state.unopened && this.state.currentSlide == 0,
          'active': this.state.currentSlide == 0 })}`}>
            <picture className={`${this.getClassNames({'beer': true,
            'opening': hold && this.state.opened && this.state.currentSlide ==  0 })}`} >
              <img width="174" height="577" src='https://spacergif.org/spacer.gif' />
            </picture>
          </div>
          <div tabIndex="1" id="bottle-2" key="bottle-2" className={`${this.getClassNames({'bottle': true,
          'active': this.state.currentSlide == 1, 'fall': hold && this.state.unopened && this.state.currentSlide == 1 })}`}>
            <picture className={`${this.getClassNames({'beer': true,
            'opening': hold && this.state.opened && this.state.currentSlide ==  1 })}`} >
              <img width="174" height="577" src='https://spacergif.org/spacer.gif' />
            </picture>
          </div>
          <div tabIndex="2" id="bottle-3" key="bottle-3" className={`${this.getClassNames({'bottle': true,
          'active': this.state.currentSlide == 2, 'fall': hold && this.state.unopened && this.state.currentSlide == 2 })}`}>
            <picture className={`${this.getClassNames({'beer': true,
            'opening': hold && this.state.opened && this.state.currentSlide ==  2 })}`} >
              <img width="174" height="577" src='https://spacergif.org/spacer.gif' />
            </picture>
          </div>
          <div tabIndex="3" id="bottle-4" key="bottle-4" className={`${this.getClassNames({'bottle': true,
          'active': this.state.currentSlide == 3, 'fall': hold && this.state.unopened && this.state.currentSlide == 3 })}`}>
            <picture className={`${this.getClassNames({'beer': true,
            'opening': hold && this.state.opened && this.state.currentSlide ==  3 })}`} >
              <img width="174" height="577" src='https://spacergif.org/spacer.gif' />
            </picture>
          </div>
          <div tabIndex="4" id="bottle-5" key="bottle-5" className={`${this.getClassNames({'bottle': true,
          'active': this.state.currentSlide == 4,'fall': hold && this.state.unopened && this.state.currentSlide == 4 })}`}>
            <picture className={`${this.getClassNames({'beer': true,
            'opening': hold && this.state.opened && this.state.currentSlide ==  4 })}`} >
              <img width="174" height="577" src='https://spacergif.org/spacer.gif' />
            </picture>
          </div>
          <div tabIndex="5" id="bottle-6" key="bottle-6" className={`${this.getClassNames({'bottle': true,
          'active': this.state.currentSlide == 5,'fall': hold && this.state.unopened && this.state.currentSlide == 5 })}`}>
            <picture className={`${this.getClassNames({'beer': true,
            'opening': hold && this.state.opened && this.state.currentSlide ==  5 })}`} >
              <img width="174" height="577" src='https://spacergif.org/spacer.gif' />
            </picture>
          </div>
        </div>
       )
  }
}

window.oncontextmenu = function() { return false; }
window.tocca({ swipeThreshold: 100 })

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    decrementTries: () => dispatch(decrementTries()),
    incrementOpened: () => dispatch(incrementOpened())
  }
}

export default connect(null, mapDispatchToProps)(GameObjects)
