const cacheName = 'cache-v1';
const resourcesToPrecahe = [
  './',
  './index.html',
  './style.css',
  './fizz.mp3',
  './fall.mp3'
];

self.addEventListener('install', event =>{
	console.log('Here service worker install event!');
	event.waitUntil(
		caches.open(cacheName)
		.then(cache => {
			return cache.addAll(resourcesToPrecahe);
		})
	);
});

self.addEventListener('fetch', event => {
	event.respondWith(caches.match(event.request)
		.then(cachedResponse => {
			return cachedResponse || fetch(event.request);
		})
	);
});
