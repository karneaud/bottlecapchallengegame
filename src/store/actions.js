export const incrementOpened = () => ({
  type: "INCREMENT_OPENED",
  content: 1
})

export const decrementTries = () => ({
  type: "DECREMENT_TRIES",
  content: 1
})

export const setTime = (time) => ({
  type: "SET_TIME",
  time
})

export const resetState = () => ({
  type: "RESET_STATE",
  reset: true
})
