const initialState = { time: '00:00:00', tries: 3, opened: 0 },
    changeState = (state, action) => {
      switch (action.type) {
        case 'INCREMENT_OPENED':
          return Object.assign({}, state, { opened: ++state.opened })
        case 'DECREMENT_TRIES':
          return Object.assign({}, state, { tries: --state.tries })
        case 'SET_TIME':
          return Object.assign({}, state, { time: action.time })
          case 'RESET_STATE':
            return Object.assign({}, state, initialState)
        default:
          return state
      }
    }

export default changeState
