import React from 'react';
import { Link } from "react-router-dom";
import image from '../images/bottlep.png';

const fallFx = window.fallFx, fizzFx = window.fizzFx;

import '../sass/start.scss';

class StartPage extends React.Component {
  constructor(props) {
      super(props);
  }

  render() {
    return (
      <section id="start" className="panel" >
        <header>
          <hgroup className="py-12">
            <h1 className="title">Get Ready</h1>
          </hgroup>
        </header>
        <article>
          <div className="p-12">
            <h2 className="sub-title">You will have</h2>
            <h1 className="title">30 seconds!</h1>
            <p>to complete the game!</p>
            <picture><img src={image} /></picture>
          </div>
        </article>
        <footer><Link to="/game" target="_self" onClick={()=>{
              fallFx.play();
              fizzFx.play();
            }} className="button enabled">Start Game</Link></footer>
        </section>
    );
  }
}

export default StartPage
