import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { resetState } from '../store/actions';
import GameObjects from '../components/GameObjects';
import Modal from 'react-responsive-modal';
import StopwatchWidget from '../components/Stopwatch';
import '../sass/game.scss';

const fallFx = window.fallFx, fizzFx = window.fizzFx

class GamePlay extends React.Component {

  constructor(props) {
      super(props);

      this.state = {
        open: true
      }
      this.stopWatch = React.createRef();
      this.onCloseModal = this.onCloseModal.bind(this)
  }

  componentDidMount() {
    this.setState({open: true })
    this.props.resetState()
  }

  componentDidUpdate(prevProps, prevState) {

     if(prevProps.tries != this.props.tries ){
       fallFx.play()
     } else if(prevProps.opened != this.props.opened) {
       fizzFx.play()
     }
   }

  componentWillReceiveProps(nextProps) {
    if(nextProps.tries <= 0) {
      this.setState({open: true })
      this.stopWatch.current.stopStopwatch();
    }
  }

  onCloseModal () {
    this.setState({ open: false });
    this.stopWatch.current.startStopwatch();
  };

  render () {

    return (
      <section  id="game" className="panel">
        <header>
          <hgroup className="p-12">
            <span id="tries" className="tries title">TRIES: <strong>{ this.props.tries }</strong></span>
            <span className="bottles title">OPENED: <strong id="score" className="opened">{ this.props.opened }</strong></span>
            </hgroup>
        </header>
        <article className="row flexbox">
          <div>
            <GameObjects/>
              <Modal closeOnOverlayClick={ false } onClose={() => false} open={this.state.open} center>
                { (this.props.tries > 0)?
                  (<div className="container"><h1>Get Ready</h1><p>Click to begin!</p><p><a href="javascript:void(0);" id="start-button" onClick={this.onCloseModal} className="button">START</a></p></div>)
                  : (
                    <div className="container">
                      <h1>Game Over</h1><p>Click to Continue!</p><p>
                        <Link id="end-button" to={{
                        pathname: '/end',
                        state: {
                          time: this.props.time,
                          opened: this.props.opened,
                          tries: this.props.tries
                        }
                        }}
                        target="_self" className="button enabled">Continue</Link>
                    </p></div>
                  )
                }
              </Modal>
          </div>
        </article>
        <footer><div className="p-12"><StopwatchWidget ref={this.stopWatch} /></div>
        </footer>
        </section>
    );
  }
}

function mapStateToProps(state) {
  return Object.assign({},state)
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    resetState: () => dispatch(resetState())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GamePlay)
