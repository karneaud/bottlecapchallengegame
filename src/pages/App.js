import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './Home';
import Help from './Help';
import EndPage from './End';
import StartPage from './Start';
import GamePlay from './Game';

import '../sass/home.scss';

class Application extends React.Component {
  render () {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={ Home } />
          <Route path="/end" component={ EndPage } />
          <Route path="/how-to" component={ Help } />
          <Route path="/start" component={ StartPage } />
          <Route path="/game" component={ GamePlay } />
        </Switch>
      </Router>
    );
  }
};

export default Application;
