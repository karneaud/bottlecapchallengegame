import React from 'react';
import { Link } from "react-router-dom";
import Siema from 'siema';
import image from '../images/bottlep.png'
import '../sass/help.scss';

class Help extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      currentSlide: 0
    }

    this.slider = null
  }

  componentDidMount() {
    this.slider = new Siema({
      selector: '.slider',
      onChange: () => {
        this.setState({ currentSlide: this.slider.currentSlide })
      }
    })
  }

  next() {
    this.slider.next()
  }

  render()  {
    return (
      <section id="instructions" className="panel">
          <header>
              <hgroup className="py-12">
                <h1 className="title">What to do?</h1>
                <h2 className="sub-title">Instructions</h2>
              </hgroup>
            </header>
            <article>
                <div>
                  <div className="slider">
                    <div className="p-12">
                      <h2>SWIPE</h2>
                      <h2><strong>RIGHT TO LEFT</strong></h2>
                      <p>at the right place at the right speed to open the bottle</p>
                      <a href="javascript:void(0)" onClick={() => this.slider.next()}><i className="fa fa-arrow-circle-right"></i></a>
                    </div>
                    <div className="p-12">
                      <p><i className="fa fa-warning"></i></p>
                      <h2>SWIPE <strong>TOO SLOW</strong></h2>
                      <p>and you may not be able open it!</p>
                      <a href="javascript:void(0)" onClick={() => this.slider.next()}><i className="fa fa-arrow-circle-right"></i></a>

                    </div>
                    <div className="p-12">
                      <p><i className="fa fa-warning"></i></p>
                      <h2>SWIPE <strong>TOO FAST</strong></h2>
                      <p>and you may not be able to open it also!</p>
                      <a href="javascript:void(0)" onClick={() => this.slider.next()}><i className="fa fa-arrow-circle-right"></i></a>
                    </div>
                    <div className="p-12">
                      <p><i className="fa fa-warning"></i></p>
                      <h2>SWIPE <strong>TOO LONG</strong></h2>
                      <p>and YES! You may not open it as well!</p>
                      <a href="javascript:void(0)" onClick={() => this.slider.next()}><i className="fa fa-arrow-circle-right"></i></a>
                    </div>
                    <div className="p-12">
                      <p><i className="fa fa-warning"></i></p>
                      <h2>SWIPE <strong>TOO SHORT</strong></h2>
                      <p>and YES again! You may not be able to open it also!</p>
                    </div>
                  </div>
                </div>
            </article>
            <footer>
              <Link to="/start" target="_self" className="button">
                { (this.state.currentSlide == 4)?  "Continue"
                : "Skip This" }
                </Link>
            </footer>
          </section>
    );
  }
}

export default Help
