import React from 'react';
import logo from '~images/bottle-open.png';
// import FacebookLoginWithButton from 'react-facebook-login';
import { Link } from 'react-router-dom';
import fizz_mp3 from '../../public/sounds/fizz.mp3';
import fall_mp3 from '../../public/sounds/fall.mp3';

global.fizzFx = window.fizzFx = new Audio(fizz_mp3);
global.fallFx = window.fallFx = new Audio(fall_mp3);
const fizzFx = window.fizzFx

const responseFacebook = (response) => {
  console.log(response);
}

class Home extends React.Component {
  
  render () {
    return (
      <section id="intro" className="panel">
        <header>
        <hgroup className="py-12">
          <h1 className="title">
            The<br/>
              #bottlecapchallenge</h1>
          <h3 className="sub-title">Mobile Game</h3>
        </hgroup>
        </header>
        <article>
          <picture>
            <img src={logo} alt='Opener Bottle Bartender'/>
          </picture>
        </article>
        <footer>
          <Link to="/how-to" target="_self" className="button" onClick={()=>fizzFx.play()}>Continue</Link>
        </footer>
      </section>
    );
  }
};

export default Home;
