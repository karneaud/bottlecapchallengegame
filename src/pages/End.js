import React from 'react';
import { Link } from "react-router-dom";
import image from '../images/bottlep.png';
import '../sass/end.scss';

const fizzFx = window.fizzFx, fallFx = window.fallFx

class EndPage extends React.Component {
  constructor(props) {
      super(props);
  }

  render() {
    return (
      <section id="end" className="panel" >
        <header>
          <hgroup className="py-12">
            <h3 className="title"><span>You have opened</span></h3>
          </hgroup>
        </header>
        <article>
            <picture><img src={image}/></picture>
            <h1 className="sub-title"><strong id="opened">{this.props.location.state.opened}</strong></h1>
            <p>bottle(s)!</p><p>
               In a time of</p>
            <h2 className="title"><strong id="time">{this.props.location.state.time}s</strong></h2>
            <a href="" className="button" id="share-button" onClick={() => {
              FB.ui({
                  method: 'share',
                  href: 'https://bottlecapchallengegame.surge.sh',
                  hashtag: "#bottlecapchallenge",
                  quote: `I opened ${this.props.location.state.opened} bottle(s) in ${this.props.location.state.time}`,
                  }, function(response){
                    console.re.log(response);
                  });
              }}>Share</a>
            <h3>OR</h3>
        </article>
        <footer>
        <Link to="/game" target="_self" className="button enabled" onClick={()=> {
            fallFx.play();
            fizzFx.play();
          }}>Play Again</Link>
        </footer>
        </section>
    );
  }
}

export default EndPage
