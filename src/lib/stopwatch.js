class Stopwatch {
  constructor() {
    let time = 0, offset, interval, tick = new Event('build') || new CustomEvent('build')

    const update = () => {
      if (this.isOn) time += delta()

      this.tickHandler(time)
      if(this.isOn) window.requestAnimationFrame(update)
    }, delta = () => {
      var now = Date.now(), timePassed = now - offset;

      offset = now;

      return timePassed;
    }

    this.tickHandler = (ev) => {}

    this.start = () => {
      offset = Date.now();
      this.isOn = true;
      requestAnimationFrame(update.bind(this))
    };

    this.stop = () => {
      this.isOn = false;
    };

    this.reset = () => {
      time = 0;
      update();
    };

    this.isOn = false;
  }
}

export default Stopwatch
